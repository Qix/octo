#!/usr/bin/env coffee
#          ▄▄· ▄▄▄▄▄      
#   ▪     ▐█ ▌▪•██  ▪     
#    ▄█▀▄ ██ ▄▄ ▐█.▪ ▄█▀▄ 
#   ▐█▌.▐▌▐███▌ ▐█▌·▐█▌.▐▌
#    ▀█▄▀▪·▀▀▀  ▀▀▀  ▀█▄▀▪ 
#        build system
#         Qix ~ 2014

# Register CoffeeScript require() extension
require 'coffee-script/register'

# Register Console Wrapper
require './lib/core/console.coffee'

# Global Octo object
global.octo =           require './lib/octo-make.js'
global.octo.config =    require './lib/octo-conf.js'

# Require
fs =                    require 'fs'
async =                 require 'async'
Registry =              require './lib/octofile/octo-registry.coffee'
wrap =                  require './lib/core/wrap.js'

# OctoMake Class
class OctoMaker
    constructor: (data, @registry) ->
        # Wrap the data
        data = wrap(data)
        
        # Get tasks
        eval(data).call(@tasks = {});
    
    resolve: (obj) ->
        # Switch type
        switch
            when typeof obj is 'object' and obj.constructor.name is 'Task'
                return [obj]
            when Array.isArray obj
                mapped = obj.map @resolve, @
                result = []
                reResolve = false
                for elem in mapped
                    if Array.isArray elem
                        result = result.concat elem
                        reResolve
                    else result.push elem
                return if reResolve then @resolve result else result
            when typeof obj is 'function' then return @resolve obj.call @tasks
            when typeof obj is 'string'
                if not @tasks[obj] then throw "not a task: #{obj}"
                return [@tasks[obj]]
            else
                throw "unknown task identifier: #{obj}"
    
    execute: (obj, callback) ->
        # Resolve
        try
            taskList = @resolve(obj).unique()
        catch err
            return callback err
        
        # Create worker function
        workerFunc = (task, workercb) ->
        
            # Compile files list
            @registry.pull.apply @registry, task.classes.concat (err, filesList) ->
                return workercb(err) if err

                # No files?
                if filesList.length is 0
                    # Warn and callback
                    console.warn "No files were collected for class(es): #{task.classes.join(', ')}"
                    return workercb()

                # Log
                console.nl()
                console.task task.name.underline, ('<' + filesList.length + '>').yellow, '->', (task.classes.join ', ').green

                # Execute the task
                task.execute filesList, workercb

        # Call in succession
        async.series (workerFunc.bind(@, task) for task in taskList), callback


# Error handler function
doError = (err) ->
    console.fatal err

# Array Unique function
Array.prototype._unique = (elem, ind, arr) ->
    return arr.indexOf(elem) is ind
Array.prototype.unique = () ->
    return this.filter(this._unique, this)

# Try to create a registry
reg = new Registry()
reg.populate (err) ->
    return doError(err) if err

    # Try to read our octomake file
    fs.readFile "OctoMake", (err, data) ->
        return doError(err) if err

        # Create a makefile
        makefile = new OctoMaker data, reg

        # Get passed arguments
        argv = process.argv.slice(2);

        # Can we possibly do something?
        if (argv.length is 0) and (not makefile.tasks.main)
            return doError "no tasks specified and no 'main' defined"

        # Execute tasks
        makefile.execute (if argv.length > 0 then argv else makefile.tasks.main), (err) ->
           doError err if err
           console.nl()
           (if err then console.fail else console.success) "Done" + (if err then ", with errors" else "!")
           return