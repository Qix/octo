/***
 * Octo configuration values
 * 
 * These values aren't really meant to be changed
 * by the user unless they know what they're doing.
 */

// Requirements
var os = require('os');

module.exports = {};
var c = module.exports; ///< Shorthand

/**
 * Whether or not to print debugging information
 */
c.debug = false;

/**
 * The mode of the temporary makefile
 *  generated after the wrapping method
 *  creates the meta-code for all makefiles
 */
c.tempFileMode = "0667";

/**
 * Enables the bypass of the in-place safeguard
 */
c.inPlace = false;

/**
 * Sets the maximum number of files to write concurrently
 *  on out() at a time
 */
c.maxOutFiles = 50;

/**
 * Sets the request child-process max
 *  This is in NO WAY a guarantee as it is up to the
 *  API function to honour this. Default is
 *  # of CPU cores
 */
c.maxChildProcesses = os.cpus().length;


// Merge octo-conf file, if there is one
try
{
    try
    {
        // Temporarily configure .conf extension
        //  to proxy .json extension
        require.extensions['.conf'] = require.extensions['.json'];
        
        // Attempt to get
        var confFile = require(process.cwd() + "/octo.conf");
        
        // Merge
        for(var key in confFile)
            if(confFile.hasOwnProperty(key))
                if(c.hasOwnProperty(key))
                    c[key] = confFile[key];
                else
                    console.warn("octo.conf: Unknown configuration variable: " + key);
    }finally
    {
        // Reset the .conf extension
        require.extensions['.conf'] = undefined;
    }
}catch(err)
{
    // Is it something other than a 'not found'?
    if(err.code !== 'MODULE_NOT_FOUND')
        // Re-throw
        throw err;
}