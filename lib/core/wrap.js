/***
 * Defines wrapMakefile() for the Octo.js
 * bin script.
 * 
 * This function wraps Makefiles with any
 * code necessarily to make them run without
 * any further requirements to the user.
 * 
 * At this time of writing, this function
 * adds a reference to the exports object
 * so that globals can be accessed.
 * 
 * This allows tasks to be defined as:
 * var mytask = octo.task("My Task")....
 * 
 * and executed with:
 * `octo mytask`
 * 
 * instead of having to define them as:
 * [module.]exports.mytask = octo.task(...)
 */
 
var header =
[
    "(function () {",
    "var task = this;"

].join("\n");
 
var footer =
[
    "(function() {",
    "    for(var tkey in task)",
    "        task[tkey].name = tkey;",
    "})();",
    
    "})"
].join("\n");
 
module.exports = function(src)
{
    // Return
    return [header, src, footer].join("\n\n");
};