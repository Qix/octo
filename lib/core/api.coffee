#
# API / API Methods
#   Defines methods and converters for managing the API registry
#

Proxy = require './proxy.coffee'

#
# Defines all meta-methods for
#   API methods
class APIMethod
    constructor: (@name) ->

    ##
    # Initializes the API method
    # @this A reference to the instance of the call
    # @return   true if the method should still be added;
    #           false if the call should be omitted from the call list
    init: () -> true


#---------------------------------------------------------------------------------------------#

wrap = (extension, name) ->
    # Apply APIMethod prototype to the API method
    extension[k] = v for k, v of APIMethod.prototype when APIMethod.prototype.hasOwnProperty k

    # Initialize, just like an instantiation
    APIMethod.call extension, name

    # Return
    return extension

class API extends Proxy
    constructor: () -> super

    set: (receiver, name, val) ->
        super receiver, name, wrap val, name
        return true


# Export
module.exports = ()-> new API().proxy()