#
# Facilitates running a command multiple times
#   without tying up too many system resources
#
#   This class allows for the use of output
#   handling -- this can be used to parse output,
#   forward to the command line, etc.


exec = (require 'child_process').exec
async = require 'async'
stream = require 'stream'
config = require '../octo-conf.js'

# Returns an array of pipe targets
#   or -1 to ignore
#
#   Elements that are null (use ===) mean to 'inherit'
resolvePipe = (dest, child) ->
    switch
        when dest instanceof Array
            result = []
            result = result.concat(resolvePipe elem) for elem in dest
            return result
        when dest instanceof stream then [dest]
        when typeof dest is 'string'
            switch dest
                when 'ignore' then -1
                when 'inherit' then [null]
                when 'stderr' then [process.stderr]
                when 'stdout' then [process.stdout]
                when 'stdin' then [process.stdin]
                else throw "unknown I/O destination: #{dest}"
        when typeof dest is 'number'
            switch dest
                when 1 then child.stdout
                when 2 then child.stderr
                when 0 then child.stdin
                else throw "unknown I/O descriptor: #{dest}"
        else null

pipeAll = (dest, child) ->
    unless dest is 'ignore'
        switch dest
            when 'inherit'
                child[chstr].pipe process[chstr] for chstr in ['stdout', 'stdin', 'stderr']
            when 'stdout'
                chstr.pipe process.stdout for chstr in [child.stdout, child.stderr]
            when 'stderr'
                chstr.pipe process.stderr for chstr in [child.stdout, child.stderr]
            when 'stdin'
                child.stdin.pipe process.stdin

class ExecQueue
    constructor: (@concurrent = config.maxChildProcesses) ->
        # Set worker function
        queueFunc = (task, queuecb) ->
            # Log
            console.cmd task.command

            # Setup callback information
            callbackInfo = {}

            # Create execution object
            child = exec task.command, task.options, (err, sout, serr) ->
                if err then queuecb err
                else queuecb null, callbackInfo.code, callbackInfo.sig
            child.on 'exit', (code, sig) ->
                callbackInfo.code = code
                callbackInfo.sig = sig

            # Map I/O
            if task.options.io?
                unless typeof task.options.io is 'object'
                    if typeof task.options.io is 'string'
                        pipeAll task.options.io, child
                    else
                        console.warn "Unknown I/O object for ExecQueue: '#{task.options.io}' <#{typeof task.options.io}>"
                else
                    # STDOUT
                    if task.options.io.hasOwnProperty("stdout")
                        resDests = resolvePipe task.options.io.stdout
                        child.stdout.pipe (if dest then dest else process.stdout) for dest in resDests when dest isnt -1
                    # STDERR
                    if task.options.io.hasOwnProperty("stderr")
                        resDests = resolvePipe task.options.io.stderr
                        child.stderr.pipe (if dest then dest else process.stderr) for dest in resDests when dest isnt -1
                    # STDIN
                    if task.options.io.hasOwnProperty("stdin")
                        resDests = resolvePipe task.options.io.stdin
                        child.stdin.pipe (if dest then dest else process.stdin) for dest in resDests when dest isnt -1


        # Create Worker
        @queue = async.queue queueFunc, @concurrent

    push: (command, options, callback) ->
        # Strange coffee doesn't do this...
        if options? and not callback?
            callback = options
            options = {}

        task = {'command': command, "options": options}
        @queue.push task, callback
        
# Exports
module.exports = ExecQueue