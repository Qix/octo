#
# Sets up a forwarding proxy base using node-proxy
#   Classes should NOT extend this, but instead
#   create a new instance, override as necessary,
#   and then call .proxy() to generate a proxy.

Proxy = require 'node-proxy'

class ForwardingProxy
    constructor: (@_ = {}) ->

    proxy: () -> Proxy.create @

    getOwnPropertyNames: () -> Object.getOwnPropertyNames @_
    getPropertyNames: () -> Object.getPropertyNames @_
    defineProperty: (name, desc) -> Object.defineProperty @_, name, desc
    delete: (name) -> delete @_[name]
    has: (name) -> name of @_
    hasOwn: (name) -> Object.prototype.hasOwnProperty.call @_, name
    get: (receiver, name) -> @_[name]
    enumerate: () -> name for name of @_
    keys: () -> Object.keys @_

    set: (receiver, name, val) ->
        @_[name] = val
        return true

    getOwnPropertyDescriptor: (name) ->
        desc = Object.getOwnPropertyDescriptor @_, name
        desc.configurable = true if desc isnt undefined
        return desc

    getPropertyDescriptor: (name) ->
        desc = Object.getPropertyDescriptor @_, name
        desc.configurable = true if desc isnt undefined
        return desc

    fix: () ->
        if Object.isFrozen @_
            result = {}
            Object.getOwnPropertyNames(@_).forEach (name) ->
                result[name] = Object.getOwnPropertyDescriptor @_, name
            return result

        # As long as obj is not frozen, the proxy
        #   won't allow itself to be fixed.
        #   This causes a TypeError.
        return undefined

module.exports = ForwardingProxy