/***
 * Provides a class structure for files
 * 
 * This is already done, but I wanted a nifty
 * method for generating tmp names on the fly
 */
 
var tmp = require("tmp"); 

// Indicate post-program cleanup
tmp.setGracefulCleanup();

function File(pathname)
{
    // Store
    this.path = pathname;
    this.current = this.path;
    this.last = this.path;
}

File.prototype.generate = function(callback)
{
    // Create scoped symbol for 'this'
    var thisObj = this;
    
    // Generate temp name
    tmp.tmpName(
        {template: "/tmp/octo-XXXXXX-" + this.current.substring(this.current.lastIndexOf("/") + 1)},
        function(err, path){
            // Error?
            if(err)
            {
                callback(err);
                return;
            }
            
            // Set + Return
            thisObj.next = path;
            callback();
        });
};

// Export
module.exports = File;