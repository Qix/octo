/***
 * Defines the `octo.api.API.prototype.bind()` method.
 * 
 * See `lib/octo-make.js` for information on
 * what exactly this does.
 */

require("./weakbind.js");

function wrapper(fn, name)
{
    // Fix arguments array
    var args = Array.prototype.slice.call(arguments, 2);
 
    // Check argument counts
    //  This will pad the arguments array with nulls
    var remaining = (fn.length - 1) - args.length;
    if(remaining > 0)
        for(var i = 0; i < remaining; i++)
            args.push(null);
    
    // Bind
    var bound = fn.bindWeak.apply(fn, args);
    
    // Create method context
    var callContext = {task: this};
    
    // Does this have an init() meta-function?
    //  If it does, call it and check the return value
    //  If it doesn't, push anyway
    if(!fn.init || typeof fn.init !== "function" || fn.init.apply(callContext, args) !== false)
        this.calls.push({"name": name, "fn": bound, "context": callContext});
    
    // Return this
    return this;
}

function binder(fn, name, thisobj)
{
    // Return
    return wrapper.bind(thisobj, fn, name);
}
 
module.exports = binder;