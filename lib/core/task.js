/***
 * Defines the `octo.task` method.
 * 
 * See `lib/octo-make.js` for information on
 * what exactly this does.
 */

// Require
var async = require("async");

// Task class
function Task()
{
    /**
     * Array of classes to include
     */
    if(arguments.length > 0 && arguments[0])
        this.classes = Array.prototype.slice.call(arguments);
    else
        this.classes = [];
    
    /**
     * Array of functions to call
     */
    this.calls = [];
}

/**
 * Executes the task
 */
Task.prototype.execute = function(fileArr, callback)
{
    // Proxy this
    var taskObj = this;
    
    // Create bound functions
    var compiledCalls = [];
    
    // Compile all functions
    for(var i = 0, len = this.calls.length; i < len; i++)
    {
        // Create the per-method context
        var callContext = {files: fileArr, task: this};
        callContext.call = this.calls[i].context;
        
        // Compile
        compiledCalls.push(this.calls[i].fn.bind(callContext));
    }
        
    // Call
    async.series(compiledCalls, function(err, results)
    {
        // Error?
        if(err)
        {
            callback("ERROR in task '" + taskObj.name + "' at " + taskObj.calls[results.length-1].name +  "(): " + err, results);
            return;
        }
        
        // Callback
        callback(err, results);
    });
};

function bindRecursive(obj, pth, task)
{
    // Create the resulting object;
    var o = {};
    
    // Iterate the source object
    for(var i in obj)
    {
        // Check has own property
        if(!obj.hasOwnProperty(i))
            continue;

        // Update path
        var ipth = pth + "." + i;
        
        // Get member
        var member = obj[i];
        
        // Get type
        var tipo = typeof member;
        if(tipo == "function")
            o[i] = octo.api.bind(member, i, task);
        else if(tipo == "object")
            o[i] = bindRecursive(member, ipth, task);
        else
            // Warn
            console.warn("WARNING: Found non-function API prototype member for task '" +
                    task.name + "': " + ipth);
    }
    
    // Return
    return o;
}

// octo.task()
module.exports = function(name)
{
    // Create task object
    var task = new Task(name);

    // Index all API members
    for(var key in octo.api)
    {
        // Get the member
        var member = octo.api[key];
        
        // Part of the prototype?
        if(member == octo.api.constructor.prototype[key])
            continue;
        
        // Check types
        var tipo = typeof member;
        if(tipo == "function")
            // Bind and create
            task[key] = octo.api.bind(member, key, task);
        else if(tipo == "object")
            // Recurse
            task[key] = bindRecursive(member, key, task);
        else
            // Warn + Continue
            console.warn("WARNING: Found non-function API prototype member for task '" +
                    task.name + "': " + key);
    }
    
    // Return
    return task;
};