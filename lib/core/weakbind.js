/**
 * WeakBind extension
 */

// Apply weak binding to function prototypes
Function.prototype.bindWeak = function()
{
    // Apply
    var args = Array.prototype.slice.call(arguments);
    var func = this;
    
    // Return
    return function()
    {
        // Get arguments
        var appargs = Array.prototype.slice.call(arguments);
        var newargs = args.concat(appargs);

        // Return
        return func.apply(this, newargs);
    };
};