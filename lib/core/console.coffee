# console.coffee
#   Modifies how the console outputs, adding
#   more specific tags for console coloring

require('colors');

# Get the prototype
construct = console.constructor
proto = construct.prototype

# Store originals
_log = proto.log;
_warn = proto.warn;
_err = proto.error;

# New-line Intenter
I = (str...) ->
  return str.join(' ').replace /(\r?\n)/g, "\n    "

# Console API
proto.log = (str...) ->
  _log.call console, I("    " + str.join(' '))

proto.imp = (str...) ->
  _log.call console, I(("-   " + str.join(' ')).bold)

proto.warn = (str...) ->
  _warn.call console, I(["!  "].concat(str).join(' ').bold.yellow)

proto.error = (str...) ->
  _err.call console, I("!!!".black.bredBG, str.join(' ').bold.red)
  console.trace() if octo? and octo.config? and octo.config.debug

proto.fail = (str...) ->
  _err.call console, I(("!   " + str.join(' ')).bold.red)

proto.success = (str...) ->
  _log.call console, I((">   " + str.join(' ')).bold.green)

proto.fatal = (str...) ->
  _err.call console, I(("!!! " + str.join(' ') + " !!!").black.bredBG)
  console.trace() if octo? and octo.config? and octo.config.debug

proto.task = (str...) ->
  _log.call console, I(("#   " + str.join(' ')).bold.cyan)

proto.cmd = (str...) ->
  _log.call console, I(("    ~  " + str.join(' ')).magenta)

proto.op = (str...) ->
  _log.call console, I(("    ~  " + str.join(' ')).cyan)

proto.file = (str...) ->
  _log.call console, I(("    -> " + str.join(' ')).black.bold)

proto.nl = () ->
  _log.call console, ''

# Create a new console

delete global.console
global['console'] = new construct(process.stdout)