#  Defines the Octo Registry generator that
#  is used to discover and manage .octo files that
#  describe the filesystem

# Require
glob        = require 'glob'
minimatch   = require 'minimatch'
async       = require 'async'
fs          = require 'fs'
File        = require '../core/file.js'

# Files Array Unique-ifier
uniqueFiles = (arr) -> arr.reduce (p, c) ->
        (return p if cc.path is c.path) for cc in p
        p.push c
        return p
    , []

# Globber Class
class OctoGlobber
    constructor: (@clazz) ->
        @adders = []
        @excluders = []
    
    applyBase: (pattern, base) ->
        return pattern unless base? and base isnt ''
        "#{base.replace(/\/+$/, '')}/#{pattern}" unless pattern.substring(0, 1) is '/'

    
    add: (pattern, base) ->
        @adders.push @applyBase pattern, base
        
    exclude: (pattern, base) ->
        @excluders.push minimatch.makeRe(@applyBase pattern, base)
        
    execute: (callback) ->
        # Files
        if @files?
            callback null, @files
            return
        
        # Set up set
        @files = []
        
        # Iterate adders
        eachFunc = (adder, eachcb) =>
            # Glob it
            glob adder, {matchBase: true}, (err, files) =>
                # Return if error
                return eachcb err if err
                
                # Reject files
                rejectFunc = (file, rejectcb) =>
                    # Does it match some?
                    someFunc = (exclusion, somecb) =>
                        somecb file.matches exclusion
                    async.some @excluders, someFunc, rejectcb
                rejectFinishFunc = (accepted) =>
                    # Map
                    mapFunc = (file, mapcb) =>
                        mapcb null, {'path': file, 'origin': @clazz}
                        
                    mapFinishFunc = (err, results) =>
                        if err
                            eachcb err
                        else
                            @files = @files.concat results || []
                            eachcb()
                            
                    async.map accepted, mapFunc, mapFinishFunc
                async.reject files, rejectFunc, rejectFinishFunc
            
        eachFinishFunc = (err) =>
            if err
                callback err
            else
                # Filter unique and callback
                @files = uniqueFiles @files
                callback null, @files
        async.each @adders, eachFunc, eachFinishFunc

# File Class
class OctoFile
    constructor: (@filename, globbers, callback) ->
        # Generate the relative pathname
        @relpath = @filename.replace /\.octo$/, ""
    
        # Selected globber placeholder
        selectedGlobber = null
        
        # Read the file
        fileReadFunc = (err, data) =>
            # Return if error
            return callback err if err
            
            # Iterate the lines
            for line in data
                    .toString()
                    .replace /\r/, ""
                    .split /\n+/
                # Trim
                line = line.trim()
                    
                # Switch the line data
                lastMatch = null
                switch
                    when line is "" then continue
                    when line.substring(0, 1) is "#" then continue
                    when lastMatch = line.match(/([\w\-]+)\s*\:/)
                        # Get Class name
                        className = lastMatch[1]
                    
                        # Create globber if necessary and then select
                        globbers[className] = new OctoGlobber(className) unless globbers[className]?
                        selectedGlobber = globbers[className]
                    when selectedGlobber?
                        # Get negation
                        line = line.replace /(\!\!)/g, ""
                        if line.substring(0, 1) is '!'
                            # Add exclusion
                            selectedGlobber.exclude line.substring 1
                        else
                            # Add addition
                            selectedGlobber.add line, @relpath
                    else
                        # Malformed .octo file
                        callback "#{@filename}: Pattern falls before class definition: #{line}"
                        return
            
            # Callback
            callback()
        fs.readFile @filename, fileReadFunc

# Registry class
class OctoRegistry
    constructor: () ->
        @globbers = {}
        @files = []
    
    populate: (callback) ->
        # Find all .octo files
        globFunc = (err, globfiles) =>
            if err
                callback err
                return
            
            # Create globbers from .octo files
            eachFunc = (file, eachcb) =>
                @files.push(new OctoFile(file, @globbers, eachcb))
                
            async.each globfiles, eachFunc, callback
        glob "**/.octo", {matchBase: true}, globFunc
        
    pull: (classes..., callback) ->
        # Concatenate
        concatFunc = (className, concatcb) =>
            # Do we have that class?
            if not @globbers[className]
                concatcb "file class not found: #{className}"
                return
                
            # Get files for that class
            @globbers[className].execute (err, rawFiles) =>
                # Return if error
                return concatcb err if err
                
                # Map to new file objects
                mapFunc = (file, mapcb) =>
                    f = new File file.path
                    f.className = file.origin
                    f.generate (err) => if err then mapcb err else mapcb null, f
                async.map rawFiles, mapFunc, concatcb
            
        async.concat classes, concatFunc, callback

# Export
module.exports = OctoRegistry
                    