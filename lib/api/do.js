/***
 * Allows the doing of a function and
 * the execution of other tasks
 */
 
function doFunc(fn, callback)
{
    // Check that fn is actually
    //  a function
    if(typeof fn != "function")
    {
        callback("Argument passed to do() not a function: " + fn);
        return;
    }
    
    // Call it with context
    fn.call(this, function(err)
    {
        if(err)
            err = "in do(): " + err;
        callback(err);
    });
}

doFunc.init = function(task)
{
    // Is it a function?
    if(typeof task == "function")
        // Return; we'll execute the dynamic version
        return true;
    
    // Must be a task!
    if(typeof task != "object" || task.constructor.name != "Task")
        // Throw
        throw "do(): not a task: " + task;
        
    // Apply call stack
    this.task.calls = this.task.calls.concat(task.calls);
    
    // Apply 
    //  TODO This is incorrect.
    this.task.classes = this.task.classes.concat(task.classes);
    
    // Omit this call (return false)
    return false;
};

// Export
module.exports = {
    "do": doFunc,
};