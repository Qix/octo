/***
 * Allows the user to modify the state
 * 
 * This is supposed to be rudimentary.
 * Anything bigger or better than this should be
 * put into its own function.
 */
 
function state(obj, callback)
{
    // Append all
    for(var key in obj)
        this[key] = obj[key];
        
    // Callback
    callback();
}

// Export
module.exports = state;