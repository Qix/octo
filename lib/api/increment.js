/***
 * Implements an incremental builder
 */
 
// Require
var fs = require('fs');
var async = require('async');
var route = require('./out.coffee').route;

function increment(callback)
{
    // Cache this
    var thisObj = this;
    
    // Try to find SOME artifacts...
    findArtifacts(this.task, function(truth)
    {
        // Did we find any?
        if(!truth)
        {
            callback();
            return;
        }
        
        // Iterate the files
        async.filter(thisObj.files, function(file, filtercb)
        {
            // Iterate artifacts
            async.someSync(thisObj.task.calls,
            function(call, sscb)
            {
                // Are there artifacts?
                if(!call.context.routings)
                {
                    sscb(false);
                    return;
                }
                
                // Get routings
                var routes = call.context.routings;
                
                // Clone file
                var cloneFile = {
                    path: file.path,
                    next: file.next,
                    current: file.current
                };
                
                // Apply the routes
                route(cloneFile, routes, function(err)
                {
                    // Error?
                    if(err)
                    {
                        sscb(err);
                        return;
                    }
                    
                    // Is it in-place?
                    //  The in-place check will take place
                    //  within out(), not here.
                    if(cloneFile.path === cloneFile.next)
                    {
                        sscb(undefined, true);
                        return;
                    }
                    
                    // Stat them
                    async.map([cloneFile.current, cloneFile.next], fs.stat, function(err, stats)
                    {
                        // Error?
                        if(err)
                        {
                            // Is the error an enoent
                            if(err.code === 'ENOENT')
                                sscb(undefined, true);
                            else
                                sscb(err);
                            return;
                        }
                        
                        // Callback!
                        sscb(undefined, stats[0].mtime.getTime() > stats[1].mtime.getTime());
                    });
                });
            },
            function(err, truthVal)
            {
                // Throw the error
                if(err) 
                    throw err;
                
                // Callback
                filtercb(truthVal);
            });
        }, function(outdated)
        {
            // Update list
            thisObj.files = outdated;
            
            // Callback!
            callback();
        });
    });
}

function findArtifacts(task, callback)
{
    // Try to find one
    async.some(task.calls, function(call, somecb)
    {
        // Check
        somecb(call.context.routings && Array.isArray(call.context.routings));
    }, callback);
}

// Synchronous some()
//  Contrary to the original some(), this *does* take an error!
async.someSync = function(collection, iterator, callback)
{
    // Cache counts
    var len = collection.length;
    var i = 0;
    var res = false;
    
    // Enter whilst
    async.whilst(
        function() { return i < len && !res; },
        function(whilstcb)
        {
            // Call iterator
            iterator(collection[i], function(err, truth)
            {
                // Error?
                if(err)
                {
                    whilstcb(err);
                    return;
                }
                
                // Set
                res = truth;
                ++i;
                whilstcb();
            });
        },
        function(err)
        {
            // Error?
            if(err)
            {
                callback(err);
                return;
            }
            
            // Callback
            callback(undefined, res);
        }
        );
};

// Export
module.exports = increment;