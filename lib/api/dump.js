/***
 * Dumps a particular part of the state
 */
 
function dump()
{
    // Get callback
    //  Oh the joys of variadic functions...
    var callback = arguments[arguments.length - 1];
    
    // Args?
    if(arguments.length === 1)
    {
        console.log("DUMP ALL:");
        console.log("---------");
        console.log(JSON.stringify(this, null, 4));
        console.log();
    }else
    {
        // Iterate
        var argLen = arguments.length - 1;
        for(var i = 0; i < argLen; i++)
        {
            var arg = arguments[i];
            console.log("DUMP:", arg)
            console.log(new Array(6 + arg.length).join("-"));
            console.log(JSON.stringify(this[arg], null, 4));
            console.log();
        }
    }
    
    // Callback
    callback();
}

// Export
module.exports = dump;