#
# Webfonts compilation
#	Requires `ttf2eot` and `ttf2woff` to be globally installed
#	Also requires batik's TTF->SVG JAR (easiest method to get it is
#	to use Ivy). The CLASSPATH environment variable should point to
#	all of the dependency JARs.

CmdLine = require 'cmd-object'
path = require 'path'
ExecQueue = require '../../core/execqueue.coffee'
async = require 'async'
File = require '../../core/file.js'

cmd = {}

cmd.svg = CmdLine("java")
	.$().always("org.apache.batik.apps.ttf2svg.Main")
	.$("in").wrap()
	.$().always("-autorange")
	.$("svgid").default("font").wrap().prefix("-id ")
	.$("out").suffix(".svg").wrap().prefix("-o ")

cmd.woff = CmdLine("ttf2woff")
	.$("in").wrap()
	.$("out").suffix(".woff").wrap()

cmd.eot = CmdLine("ttf2eot")
	.$("in").wrap()
	.$("out").suffix(".eot").wrap()

webfontCompile = (formats, options, callback) ->
	# Fix arguments
	if arguments.length is 2
		callback = options
		options = {}

	# Setup default options
	options['formats'] = formats

	# Create execution queue
	exec = new ExecQueue()

	# Setup another array that holds ALL files
	newFiles = []

	# Iterate files
	async.eachSeries @files
		, (file, eachcb) =>
			# Setup options
			opts = {}
			opts[k] = v for k,v of options when options.hasOwnProperty k
			opts.in = file.current
			opts.out = file.next.replace /\.[^\/]+$/, ""

			# Call for each selected type
			async.eachSeries opts.formats
				, (format, feachcb) =>
					# TTF?
					if format is 'ttf'
						newFiles.push file
						return feachcb()

					# Generate the command line
					cmd[format].generate opts, (err, cmdLine) ->
						# Err if necessary
						return feachcb err if err

						# Execute
						exec.push cmdLine, (err, code) ->
							# Err if necessary
							return feachcb err if err

							# Check code
							return feachcb "Webfont <#{format}> generation failed with code #{code}: #{file.current}" unless code is 0

							# Generate actual file name
							newFile = new File file.path
							newFile.current = "#{opts.out}.#{format}"
							newFile.last = file.last

							# Push onto new files array
							newFiles.push newFile

							# Generate next temp filename
							newFile.generate feachcb
				, eachcb
		, (err) =>
			# Die with an error if necessary
			return callback err if err

			# Set our new files array and callback
			@files.pop() while @files.length isnt 0
			@files.push file for file in newFiles
			callback()

# Export
module.exports = {
	"compile": {
		"webfont": webfontCompile.bindWeak ['woff', 'eot', 'svg', 'ttf']
		"eot": webfontCompile.bindWeak ['eot']
		"svg": webfontCompile.bindWeak ['svg']
		"woff": webfontCompile.bindWeak ['woff']
	}
}
