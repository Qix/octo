#
# Java Compiler (JavaC) handler
#

# Require
CmdLine = require 'cmd-object'
exec = (require 'child_process').exec;
path = require 'path'
async = require 'async'
CmdQueue = require '../../../core/execqueue.coffee'
stream = require 'stream'
fs = require 'fs'
base32 = require 'base32'
path = require 'path'
mkdirp = require 'mkdirp'
os = require 'os'

# Package line regex
packageRegex = /^\s*package\s*((?:[\w_]+\.)*[\w_]+)\s*;$/m

# Determine javac
isWin = os.platform() is 'win32'
javacLocation = process.env['JAVA_HOME'] + '/bin/javac.exe' if isWin
javacLocation = process.env['JAVA_HOME'] + '/bin/javac' unless isWin

# Javac Command Line Pipeline
# http://docs.oracle.com/javase/7/docs/technotes/tools/windows/javac.html
javacCommand = CmdLine("\"#{javacLocation}\"")
    .$("defines").default({}).pairs().prefix("-D")
    .$("annotation-options").default({}).pairs().prefix("-A")
    .$("classpath").join(path.delimiter).wrap().prefix("-cp ")
    .$("bin").wrap().prefix("-d ")
    .$("deprecation").flag("-deprecation")
    .$("encoding").prefix("-encoding ")
    .$("ext").join(path.delimiter).wrap().prefix("-extdirs ")
    .$("debug").flag("-g")
    .$("nodebug").flag("-g:none")
    .$("debug-type").join(",").prefix("-g:")
    .$("implicit").prefix("-implicit:")
    .$("java-options").prefix("-J")
    .$("nowarn").flag("-nowarn")
    .$("processing").prefix("-proc:")
    .$("processors").join(",").prefix("-processor ")
    .$("processor-path").join(path.delimiter).wrap().prefix("-processorpath ")
    .$("src-gen").wrap().prefix("-s ")
    .$("source-version").prefix("-source ")
    .$("sourcepath").join(path.delimiter).wrap().prefix("-sourcepath ")
    .$("verbose").flag("-verbose")
    .$("fatal-warnings").flag("-Werror")
    .$("extended")
    .$("target-version").prefix("-target ")
    .$("bootclasspath").join(path.delimiter).wrap().prefix("-bootclasspath ")
    .$("files").wrap()

# Create generation pool
pool = new CmdQueue()

# Javac parser
javacParser = (results) ->
    do(stream) ->
        stream = new stream.Writable()
        stream._write = (chunk, encoding, callback) ->
            # TODO Implement stream
            callback()
        return stream

# Temp location generator
#   We add a random hash since
#   we could be building several projects
#   with the same package/classes
generateTmpDir = (base = "/tmp/octo-javac") ->
    path.resolve path.join(base, base32.encode(String(Math.floor(Math.random() * 100000))))

shallowClone = (obj) ->
    nobj = {}
    nobj[key] = val for key, val of obj when obj.hasOwnProperty key
    nobj

# Javac Function
javac = (options = {}, callback) ->
    # Check for javac
    console.warn("JAVA_HOME isn't set! Java compiling might not run properly...") unless process.env['JAVA_HOME']
    return callback("javac not found; did you set JAVA_HOME?") unless fs.existsSync(javacLocation)

    # Setup bin
    options.bin = options.bin || generateTmpDir()

    # Make our temporary directory
    mkdirp options.bin, (err) =>
        if err
            callback err
            return

        # Iterate files
        async.map @files
            , (file, mapcb) =>

                # Read the file so we can calculate the final
                #   javac output file
                fs.readFile file.current, (err, data) ->
                    # Set up results object
                    results = {
                        errors: [],
                        warnings: [],
                        successful: true,
                        file: file
                    }

                    # Set up this options
                    thisOptions = shallowClone options

                    # Find package
                    data = data.toString()
                    offset = data.search packageRegex
                    unless offset is -1
                        match = data.substring(offset).match packageRegex
                        pkgPath = match[1].replace '.', '/'
                        oldFilename = file.current.substring(file.current.lastIndexOf "/")
                        newFilename = oldFilename.replace /\.java$/, ".class"

                        # Translate
                        results.finalPath = path.join options.bin
                            , pkgPath
                            , newFilename

                        # Add source path, if necessary
                        if not thisOptions['sourcepath']?
                            thisOptions['sourcepath'] = ['.']

                            # Calculate the package+file path
                            srcPath = file.current.replace("/#{pkgPath}#{oldFilename}", "");

                            # Add
                            thisOptions['sourcepath'].push srcPath
                    else
                        results.finalPath = path.join options.bin
                            , ((file.current.substring(file.current.lastIndexOf("/"))).replace(/\.java$/, ".class"))

                        # Add source path, if necessary
                        if not thisOptions['sourcepath']?
                            thisOptions['sourcepath'] = ['.']
                            srcPath = file.current.replace(file.current.substring(file.current.lastIndexOf("/")), "")
                            thisOptions['sourcepath'].push(srcPath)

                    # Bind parser
                    parser = javacParser results

                    # Create options
                    opts = {
                        "io": {
                            "stdout": [javacParser, 'inherit'],
                            "stderr": [javacParser, 'inherit']
                        }
                    }

                    # Merge options
                    opts[key] = val for key, val of @javac if @javac

                    # Generate our command line
                    javacCommand.generate thisOptions
                    , (err, cmdline) =>
                            if err
                                callback err
                                return

                            # Make final command line
                            fileCommand = "#{cmdline} \"#{file.current}\""

                            # Execute!
                            pool.push fileCommand
                                , @javac || {}
                                , (err, code) ->
                                    if err
                                        mapcb err
                                        return
                                    unless code is 0
                                        results.successful = false
                                    mapcb null, results
            , (err, mapped) ->
                if err
                    callback err
                    return

                # Iterate the files and update their
                #   current directories accordingly
                async.each mapped
                , (result, eachcb) ->
                    # TODO output warnings!
                    if result.successful
                        result.file.current = result.finalPath
                        result.file.generate eachcb
                    else
                        # TODO output errors!
                        eachcb()
                , callback

# Export
module.exports = {
    "compile": {
        "java": javac
    }
}