/***
 * Defines the typescript compilation API function
 * 
 * See [https://www.npmjs.org/package/ts-compiler] for
 * a full list of options. skipWrite will always be true.
 */

// Require
var fs = require("fs");
var async = require("async");

var tsc;
var present = false;
try
{
    require.resolve("ts-compiler");
    present = true;
}catch(e){}

function compileTypescript()
{
    // Get callback
    var callback = arguments[arguments.length - 1];
    
    // Are there options?
    var options = {};
    if(arguments.length > 1)
        options = arguments[0];
        
    // Force skipWrite
    options.skipWrite = true;
    
    // Compile
    async.each(this.files,
    function(file, eachcb)
    {
        try
        {
            // Compile the file
            console.op("TypeScript:", file.current);
            tsc.compile([file.current], options)
                .done(function(results)
                {
                    // Write the file
                    //  TODO Write extraneous files?
                    fs.writeFile(file.next, results[0].text, function(err)
                    {
                        // Error?
                        if(err)
                        {
                            eachcb(err);
                            return;
                        }
                        
                        // Forward the file
                        file.current = file.next;
                        file.generate(eachcb);
                    });
                });
        }catch(e)
        {
            // Handle
            eachcb("TypeScript compile error: " + file.current + "\n\t- " + e + "\n\t\tfrom: " + file.path);
            return;
        }
    },
    callback);
}

// Export if present
if(present)
{
    tsc = require("ts-compiler");
    module.exports = {
        "compile": {
            "typescript": compileTypescript
        }
    };
}else
    module.exports = {};