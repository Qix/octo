/***
 * Defines coffeescript functionality
 * 
 * For these functions to be available,
 * you must have `coffee-script` installed
 * in a place Node can find it!
 */

// Require
var async = require("async");
var fs = require("fs");

var coffeescript;
var present = false;

// Detect coffee-script 
try
{
    require.resolve("coffee-script");
    present = true;
}catch(e)
{}

function preprocessCoffee(path, original, imp, callback)
{
    // Generate regex
    var reg = /^\s*\@?import\s+([\w\-\.]+)\s*\;?$/mg;
    
    // Read the file contents
    fs.readFile(path, function(err, data)
    {
        // Error?
        if(err)
        {
            if(err.code === 'ENOENT')
                return callback("Import/script not found in '" + original + "': " + (imp ? imp : path));
            return callback(err);
        }
        
        // Reset data
        //  We append a new line here
        //  due to a bug in the coffee-script compiler
        //  where the first line is ignored. Not
        //  sure why this is.
        data = "\n" + data.toString();
        
        // Setup matches
        var imports = [];
        
        // Find all matches
        var matches;
        while((matches = reg.exec(data)) !== null)
            imports.push(matches[1]);
            
        // Get the contents of those imports
        var importData = {};
        async.each(imports, function(imp, eachcb)
        {
            // Generate relative path
            var relPath = path.replace(/\/?[^\/]+$/, "/") + imp.replace(/\./g, "/") + ".coffee";
            
            // Is it the same as this path?
            if(relPath == path)
            {
                eachcb("Script is importing itself!");
                return;
            }
            
            // Recurse Process
            preprocessCoffee(relPath, path, imp, function(err, pdata)
            {
                // Error?
                if(err)
                {
                    eachcb(err);
                    return;
                }
                
                // Set the data
                importData[imp] = pdata;
                
                // Callback
                eachcb();
            });
        },
        function(err)
        {
            // Error?
            if(err)
            {
                callback(err + "\n\tin " + path);
                return;
            }

            // Perform the matching
            reg = /^\s*\@?import\s+([\w\-\.]+)\s*\;?$/mg;
            while((matches = reg.exec(data)) !== null)
            {
                // Get the data
                var i = importData[matches[1]];

                // Replace the data
                data = data.slice(0, matches.index) + i + data.substring(matches.index + matches[0].length);
                
                // Adjust last index (optimization)
                reg.lastIndex = matches.index + i.length;
            }
            
            // Callback!
            callback(undefined, data);
        });
    });
}

function compileCoffee()
{
    // Get callback
    var callback = arguments[arguments.length - 1];
    
    // Are there options?
    var options = {};
    if(arguments.length > 1)
        options = arguments[0];
        
    // Iterate files
    async.each(this.files, function(file, eachcb)
    {
        // Preprocess (and get data)
        preprocessCoffee(file.current, file.current, undefined, function(err, data)
        {
            // Error?
            if(err)
            {
                eachcb(err);
                return;
            }
            
            // Compile the source
            var compiled;
            try
            {
                console.op("CoffeeScript:", file.current);
                compiled = coffeescript.compile(data.toString(), options);
            }catch(e)
            {
                // Handle
                eachcb("CoffeeScript compile error: " + file.current + "\n\t- " + e + "\n\t\tfrom: " + file.path);
                return;
            }
            
            // Write
            fs.writeFile(file.next, compiled, function(err)
            {
                // Error?
                if(err)
                {
                    eachcb(err);
                    return;
                }
                
                // Update file
                file.current = file.next;
                file.generate(eachcb);
            });
        });
    },
    function(err)
    {
        // Error?
        if(err)
        {
            callback(err);
            return;
        }
        
        // Callback!
        callback();
    });
}
    

// Export (if available)
if(present)
{
    coffeescript = require("coffee-script");
    module.exports = {
        "compile": {
            "coffee": compileCoffee
        }
    };
}else{
    module.exports = {};
}