#
# JS/HTML/CSS Minifier
#   Exposed as a compiler
#   Must have `node-minify` installed
#
#   NOTE: The default CSS compressor uses
#           the algorithm that produced
#           the best results.
#
#         The default JS compressor uses
#           the implementation that does not
#           require Java to be installed.
#
#         These things can, of course, be overridden.

# Require
async = require "async"
fs = require "fs"

# Try to find minify
try
    minifier = require.resolve 'node-minify'
    present = true
catch e

minify = (defType, options = {}) ->
    # Get callback
    callback = arguments[arguments.length - 1]

    # Correct defaults
    options.type = options.type || defType

    # Iterate files
    async.each @files
        , (elem, eachcb) ->
            # Set up file options
            options.fileIn = elem.current
            options.fileOut = elem.next
            options.callback = (err) ->
                # Error?
                eachcb err if err

                # Generate next
                elem.current = elem.next
                elem.generate eachcb

            # Compile
            console.op "Minify <#{options.type}>:", elem.current
            new minifier.minify options
        , callback

# Check for minify presence
if present
    minifier = require 'node-minify'
    module.exports = {
        "compile": {
            "js": minify.bindWeak 'uglifyjs'
            "css": minify.bindWeak 'clean-css'
        }
    }
else
    module.exports = {}