#
# Artifact/target file writing mechanism
#

# Require
fs = require 'fs'
async = require 'async'
mkdirp = require 'mkdirp'

out = (args..., callback) ->
    # Get routings
    routings = @call.routings;

    # Create queue
    #   We use a queue because it limits the number
    #   of concurrent writes due to process limitations
    #   for open file handles at a time.
    queue = async.queue (task, queuecb) ->
        # Get the action/file
        action = task.action
        file = task.file

        # Echo
        console.file file.next

        # Write it
        action file.current, file.next, (err) ->
            # Error?
            return queuecb err if err

            # Setup next files
            file.current = file.next
            file.generate queuecb
    , octo.config.maxOutFiles

    # Route all files
    async.each @files, (file, eachcb) ->
        # Store original next
        originalNext = file.next

        # Route
        route file, routings, (err) ->
            # Error?
            return eachcb err if err

            # Check for safeguard bypass
            if not octo.config.inPlace and file.next is file.path
                # Warn
                console.warn "Ignoring in-place write: #{file.next}"

                # Reset next and callback
                file.next = originalNext
                return eachcb()

            # Determine file action
            #   This prevents moving the original file
            action = (if file.current is file.path then fs.copyFile else fs.moveFile)

            # Setup makedir callback
            onMakeDir = (err) ->
                # Error?
                return eachcb() err if err

                # Add to queue
                queue.push {
                    "action": action
                    "file": file
                }

            # Get last index of /
            lastSlash = file.next.lastIndexOf '/'

            # If there is a /, break off the filename and make the parent directories
            if lastSlash isnt -1 then mkdirp (file.next.substring 0, lastSlash), onMakeDir

            # Else, just continue
            else onMakeDir()

            # Next!
            eachcb()
    , (err) ->
        # Specify drained function
        queue.drain = callback.bind @, err

out.init = (args...) ->
    # Create routings
    @routings = []

    # In-Place?
    if args.length is 0
        # Safe-guard bypass?
        if not octo.config.inPlace
            # Warn and return
            console.warn "In-Place safeguard is in place and out() was called with no arguments for task; ignoring"
            return false;
        else
            # Add our routing
            @routings.push [/^(.+)$/, '$1']

    # Simple? (dir[, ext])
    else if 1 <= args.length <= 2 and typeof args[0] is 'string'
        # Clean the destination
        dest = args[0].replace /\/+$/g, ''

        # Extension?
        if args.length is 2 and typeof args[1] is 'string'
            @routings.push [
                /^(([^\/]+)*[^\.]*)(\..*)?$/,
                "#{dest}/$1.#{args[1].replace(/^\.+/g,'')}"
            ]
        else
            @routings.push [
                /^(.+)\.[^\/]+$/,
                "#{dest}/$1..."
            ]

    # Complex Routing?
    else
        for arg in args
            if arg instanceof Array and arg.length >= 2
                @routings.push arg
            else
                console.warn "Invalid artifact grouping: #{arg}"

    # Return
    return true

# Export out
module.exports = out

route = (file, routes, callback) ->
    # Store next's extension
    nextExtension = file.next.substring(file.next.search(/\.[^\/]+$/))

    # Reset to last path
    file.next = file.last

    # Simple replacement?
    return callback if routes.length is 0

    # Iterate routes
    for rt in routes
        # Replace
        if file.next.search rt[0] isnt -1
            # Compile and continue
            file.next = file.last = file.next
                .replace rt[0], rt[1]
                .replace '$0', file.next
                .replace '$-1', file.path
                .replace '...', nextExtension

    # Callback
    callback()

# Specify route function
out.route = route

alternate_move = (origin, destination, callback) ->
    fs.copyFile origin, destination, () ->
        fs.unlink origin, callback

fs.moveFile = (origin, destination, callback) ->
    fs.rename origin, destination, (err) ->
        return alternate_move origin, destination, callback if err
        return callback()

fs.copyFile = (origin, destination, callback) ->
    read = fs.createReadStream origin
    write = fs.createWriteStream destination
    write.on 'close', callback
    read.pipe write