/***
 *           ▄▄· ▄▄▄▄▄
 *    ▪     ▐█ ▌▪•██  ▪
 *     ▄█▀▄ ██ ▄▄ ▐█.▪ ▄█▀▄
 *    ▐█▌.▐▌▐███▌ ▐█▌·▐█▌.▐▌
 *     ▀█▄▀▪·▀▀▀  ▀▀▀  ▀█▄▀▪
 *         build system
 *          Qix ~ 2014
 *
 * Octo uses a few tricks to achieve user
 * friendliness and extensibility.
 *
 * A few assumptions are to be made by the user;
 * all extension methods must be registered by the
 * user scripts *before* defining tasks.
 *
 * These methods are then indexed, wrapped and
 * used to create a new task proxy object returned
 * by entry methods.
 *
 * When registered methods are called, they are bound
 * and a reference is appended to a list to be
 * executed later.
 *
 * After all tasks have been defined (Octo automatically
 * adds defined tasks to modules.exports), Octo
 * acts much like gnumake and either executes the first
 * task or the passed tasks on the command line.
 */

// API Definitions
// - Class
function API() {
}
module.exports.api = new API();
API.prototype.bind = require("./core/bind.js");

// - Commons Tables
module.exports.api.compile = {};
module.exports.api.link = {};
module.exports.api.lint = {};

// Task method
module.exports.task = require("./core/task.js");

// Merge helper function
//  NOTE: Only recurses one level in (for commons tables)
function mergeApi(name) {
    // Get object
    var obj = require(name);

    // Iterate
    for (var i in obj) {
        // Get some info
        var memberSrc = obj[i];
        var memberDest = module.exports.api[i];
        var tipoSrc = typeof memberSrc;
        var tipoDest = typeof memberDest;

        // Different types?
        if (tipoSrc != "object" || tipoDest != "object") {
            // Warn if necessary
            if (tipoDest == "object")
                console.log("WARNING: overwriting API common table from '" + name + "': " +
                    i + " <" + tipoSrc + ">");
            module.exports.api[i] = memberSrc;
        }
        else
            // Merge the subtable
            for (var ni in memberSrc)
                module.exports.api[i][ni] = memberSrc[ni];
    }
}

/////////////////////
//-- API METHODS --//
/////////////////////

// - state() and dump()
module.exports.api.state = require("./api/state.js");
module.exports.api.dump = require("./api/dump.js");

// - out()
module.exports.api.out = require("./api/out.coffee");

// - increment() <incremental building filter>
module.exports.api.increment = require("./api/increment.js");

// - execution funcs (execute, do)
mergeApi("./api/do.js");

// - CoffeeScript Methods
mergeApi("./api/lang/coffee.js");

// - TypeScript Methods
mergeApi("./api/lang/typescript.js");

// - Java Methods
mergeApi("./api/lang/java/javac.coffee");

// - Web Minify Methods
mergeApi("./api/lang/mini-web.coffee");

// - WebFonts
mergeApi("./api/lang/webfont.coffee");